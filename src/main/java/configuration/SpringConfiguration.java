package configuration;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;

/**
 * Created by emadnikkhouy on 24/09/16.
 */

@SpringBootApplication
@ComponentScan("controller, service, model")
@PropertySource("application.properties")
public class SpringConfiguration {

}
