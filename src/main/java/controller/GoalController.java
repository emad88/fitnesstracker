package controller;

import model.Goal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Created by emadnikkhouy on 24/09/16.
 */
@RestController
@SessionAttributes("goal")
public class GoalController {

    @RequestMapping(value = "/goal", method = RequestMethod.GET)
    public String addGoal(Model model) {
        Goal goal = new Goal();
        goal.setMinutes(10);
        model.addAttribute("goal", goal);

        return "addGoal";
    }

    @RequestMapping(value = "/goal", method = RequestMethod.POST)
    public String updateGoal(@Valid @ModelAttribute("goal") Goal goal, BindingResult result) {

        System.out.println("result has errors: " + result.hasErrors());

        System.out.println("Goal set: " + goal.getMinutes());

        if(result.hasErrors()) {
            return "addGoal";
        }

        return "redirect:index.html";
    }
}
