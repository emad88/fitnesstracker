package controller;

import model.Activity;
import model.Exercise;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import service.ExerciseService;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by emadnikkhouy on 24/09/16.
 */
@RestController
public class MinutesController {

    @Autowired
    private ExerciseService exerciseService;

    @RequestMapping(value = "/addMinute",  method = RequestMethod.GET)
    public String getMinutes(@ModelAttribute("exercise") Exercise exercise) {

        return "addMinutes";
    }

    @RequestMapping(value = "/addMinute",  method = RequestMethod.POST)
    public String addMinutes(@Valid @ModelAttribute ("exercise") Exercise exercise, BindingResult result) {

        System.out.println("exercise: " + exercise.getMinutes());
        System.out.println("exercise activity: " + exercise.getActivity());

        if(result.hasErrors()) {
            return "addMinutes";
        }

        return "addMinutes";
    }

    @RequestMapping(value = "/activities", method = RequestMethod.GET)
    public List<Activity> findAllActivities() {
        return exerciseService.findAllActivities();
    }
}
