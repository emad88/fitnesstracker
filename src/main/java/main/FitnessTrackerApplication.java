package main;

import configuration.SpringConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * Created by emadnikkhouy on 24/09/16.
 */
public class FitnessTrackerApplication {

    public static void main(String[] args) {
        SpringApplicationBuilder builder = new SpringApplicationBuilder(SpringConfiguration.class);
        builder.headless(false);
        ConfigurableApplicationContext applicationContext = builder.run(args);
    }
}
