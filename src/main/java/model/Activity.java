package model;

/**
 * Created by emadnikkhouy on 24/09/16.
 */
public class Activity {

    private String desc;

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
