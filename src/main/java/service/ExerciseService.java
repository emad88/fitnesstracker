package service;

import model.Activity;

import java.util.List;

/**
 * Created by emadnikkhouy on 24/09/16.
 */
public interface ExerciseService {

    List<Activity> findAllActivities();
}
