app.controller("controller", function ($scope, $http, $window, $log) {



    $scope.about = function () {
        $window.location.href = '/about.html';
    };

    $scope.addGoal = function () {
        $window.location.href = '/addGoal.html';
    };

    $scope.addExerciseMinutes = function () {
        $window.location.href = '/addMinutes.html';
    };

});

app.controller("addGoalController", function ($scope, $window, $location) {
    $scope.placeHolder = "Enter Minutes";
    $scope.home = function () {
        $window.location.href = '/index.html';
    }
    //var username = $location.search().username;
    //$scope.username = username;
    //$scope.names = data;
    //$scope.home = function () {
    //    $window.location.href = '/index.html';
    //}
});

app.controller("addMinutesController", function ($scope, $window, $location, $log) {
    $scope.activities = ["Run", "Bike", "Swim"];


    $scope.home = function () {
        $window.location.href = '/index.html';
    };
    $scope.update = function () {
        $log.debug("selected this: " + $scope.selectedActivity);
    };
});

app.controller("aboutController", function ($scope, $window) {
    $scope.home = function () {
        $window.location.href = '/index.html';
    }
});